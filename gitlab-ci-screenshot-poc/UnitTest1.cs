using System;
using Xunit;
using Xunit.Abstractions;

namespace gitlab_ci_screenshot_poc
{
    public class UnitTest1
    {
        public UnitTest1(ITestOutputHelper output)
        {
            Output = output;
        }

        public ITestOutputHelper Output { get; }

        [Fact]
        public void Test1()
        {
            Output.WriteLine("[[ATTACHMENT|/absolute/path/to/some/file]]");
        }
    }
}
